using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InteractionUI : MonoBehaviour
{
    private GameObject btnSceneTown;
    private static InteractionUI instance; 
    public static InteractionUI Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<InteractionUI>();
            }
            return instance; 
        }
    }
    void Start()
    {
        InitUI();
        SetActions();
    }
    
    void InitUI()
    {
        btnSceneTown = GameObject.Find("BtnSceneTown");
    }

    void SetActions()
    {
        btnSceneTown.GetComponent<Button>().onClick.AddListener(SceneTownHandler);
    }

    void SceneTownHandler()
    {
        SceneManager.LoadScene(SceneConfig.experience);
    } 
}
