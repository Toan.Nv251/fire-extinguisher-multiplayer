using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCanvas : MonoBehaviour
{
    [SerializeField] private CreateRoomCanvas createRoomCanvas;
    public CreateRoomCanvas CreateRoomCanvas { get { return createRoomCanvas; } }
    [SerializeField] private CurrentRoomCanvas currentRoomCanvas;
    public CurrentRoomCanvas CurrentRoomCanvas { get { return currentRoomCanvas; } }
    private void Awake()
    {
        FirstInitialize();
    }
    private void FirstInitialize()
    {
        CreateRoomCanvas.FirstInitialize(this);
        CurrentRoomCanvas.FirstInitialize(this);
    }
}
