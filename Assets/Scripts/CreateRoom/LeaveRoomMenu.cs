using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.Threading.Tasks;

public class LeaveRoomMenu : MonoBehaviour
{
    private RoomCanvas roomCanvas;
    public void FirstInitialize(RoomCanvas canvases)
    {
        roomCanvas = canvases;
    }
    public void OnClickLeaveRoom()
    {
        PhotonNetwork.LeaveRoom(true);
        roomCanvas.CurrentRoomCanvas.hide();
    }
}
