using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Connection : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject firstLoadData;
    private void Awake()
    {
        Application.targetFrameRate = 60;
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Connection");
        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }
    private void Start()
    {
        Debug.Log("Connecting to server");
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = MasterManager.Instance.GameSetting.GameVersion;
        PhotonNetwork.NickName = MasterManager.Instance.GameSetting.NickName;
        PhotonNetwork.ConnectUsingSettings(); // kết nối và sử dụng cài đặt photon
        firstLoadData.SetActive(true);
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to server");
        print("Player Name: " + PhotonNetwork.LocalPlayer.NickName);
        if (!PhotonNetwork.InLobby)
            PhotonNetwork.JoinLobby();
        firstLoadData.SetActive(false);
    }
    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Disconnected to Server " + cause.ToString());
    }
}
