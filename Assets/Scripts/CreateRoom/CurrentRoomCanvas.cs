using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentRoomCanvas : MonoBehaviour
{
    [SerializeField] private PlayerListingMenu playerListingMenu;
    [SerializeField] private LeaveRoomMenu leaveRoomMenu;
    public LeaveRoomMenu LeaveRoomMenu {get {return leaveRoomMenu;}}
    private RoomCanvas roomCanvas;
    public void FirstInitialize(RoomCanvas canvases)
    {
        roomCanvas = canvases;
        playerListingMenu.FirstInitialize(canvases);
        leaveRoomMenu.FirstInitialize(canvases);
    }
    public void show()
    {
        gameObject.SetActive(true);
    }
    public void hide()
    {
        gameObject.SetActive(false);
    }
}
