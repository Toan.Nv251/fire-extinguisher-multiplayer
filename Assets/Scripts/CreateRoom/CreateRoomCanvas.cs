using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateRoomCanvas : MonoBehaviour
{
    [SerializeField] private CreateRoomMenu createRoomMenu;
    [SerializeField] private RoomListingMenu roomListingMenu;
    private RoomCanvas roomCanvas;
    public void FirstInitialize(RoomCanvas canvas)
    {
        roomCanvas = canvas;
        createRoomMenu.FirstInitialize(canvas);
        roomListingMenu.FirstInitialize(canvas);
    }
}
