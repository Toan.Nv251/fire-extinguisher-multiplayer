using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class RoomListingMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private Transform positionInstanceRoom;
    [SerializeField] private RoomListing roomListing;
    [SerializeField] private List<RoomListing> listings = new List<RoomListing>();
    private RoomCanvas roomCanvas;
    public override void OnJoinedRoom()
    {
        roomCanvas.CurrentRoomCanvas.show();
        foreach (Transform child in positionInstanceRoom.transform)
        {
            Destroy(child.gameObject);
        }
        listings.Clear();
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach (RoomInfo info in roomList)
        {
            //removed from rooms list
            if (info.RemovedFromList)
            {
                int index = listings.FindIndex(x => x.RoomInfo.Name == info.Name);
                if (index != -1)
                {
                    Destroy(listings[index].gameObject);
                    listings.RemoveAt(index);
                }
            }
            //added to rooms list
            else
            {
                int index = listings.FindIndex(x => x.RoomInfo.Name == info.Name);
                if (index == -1)
                {
                    RoomListing listing = Instantiate(roomListing, positionInstanceRoom);
                    if (listing != null)
                    {
                        listing.SetRoomInfo(info);
                        listings.Add(listing);
                    }
                }
            }
        }
    }
    public void FirstInitialize(RoomCanvas canvases)
    {
        roomCanvas = canvases;
    }
}
