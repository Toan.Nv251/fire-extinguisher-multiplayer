using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;

public class PlayerListing : MonoBehaviour
{
    [SerializeField] private TMP_Text namePlayer;
    public Player PlayerInfo { get; private set; }
    public void SetPLayerInfo(Player player)
    {
        PlayerInfo = player;
        SetPlayerText(player);
    }
    private void SetPlayerText(Player player)
    {
        namePlayer.text = player.NickName;
    }
}
