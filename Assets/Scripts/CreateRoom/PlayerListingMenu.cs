using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class PlayerListingMenu : MonoBehaviourPunCallbacks
{
    [SerializeField] private Transform positionInstancePlayer;
    [SerializeField] private PlayerListing playerListing;
    [SerializeField] private List<PlayerListing> listingPlayer = new List<PlayerListing>();
    private RoomCanvas roomCanvas;

    public override void OnEnable()
    {
        base.OnEnable();
        GetCurrentRoomPlayers();
    }
    public override void OnDisable()
    {
        base.OnDisable();
        for (int i = 0; i < listingPlayer.Count; i++)
        {
            Destroy(listingPlayer[i].gameObject);
        }
        listingPlayer.Clear();
    }
    public void FirstInitialize(RoomCanvas canvases)
    {
        roomCanvas = canvases;
    }
    public override void OnLeftRoom()
    {
        foreach(Transform child in positionInstancePlayer.transform)
        {
            Destroy(child);
        }
    }
    private void GetCurrentRoomPlayers()
    {
        if (!PhotonNetwork.IsConnected)
        {
            return;
        }
        if (PhotonNetwork.CurrentRoom == null || PhotonNetwork.CurrentRoom.Players == null)
        {
            return;
        }


        foreach (KeyValuePair<int, Player> playerInfo in PhotonNetwork.CurrentRoom.Players)
        {
            AddPlayerListing(playerInfo.Value);
        }
    }
    private void AddPlayerListing(Player player)
    {
        PlayerListing listing = Instantiate(playerListing, positionInstancePlayer);
        if (listing != null)
        {
            listing.SetPLayerInfo(player);
            listingPlayer.Add(listing);
        }
    }
    // public override void OnMasterClientSwitched(Player newMasterClient)
    // {
    //     roomCanvas.CurrentRoomCanvas.LeaveRoomMenu.OnClickLeaveRoom();
    // }
    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        base.OnMasterClientSwitched(newMasterClient);
        roomCanvas.CurrentRoomCanvas.LeaveRoomMenu.OnClickLeaveRoom();
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        PlayerListing listing = Instantiate(playerListing, positionInstancePlayer);
        if (listing != null)
        {
            listing.SetPLayerInfo(newPlayer);
            listingPlayer.Add(listing);
        }
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        int index = listingPlayer.FindIndex(x => x.PlayerInfo == otherPlayer);
        if (index != -1)
        {
            Destroy(listingPlayer[index].gameObject);
            listingPlayer.RemoveAt(index);
        }
    }
    public void OnClickStartGame()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.CurrentRoom.IsOpen = true;
            PhotonNetwork.CurrentRoom.IsVisible = true;
            PhotonNetwork.LoadLevel("Experience");
        }
    }
}
