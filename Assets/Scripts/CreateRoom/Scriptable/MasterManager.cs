using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEditor;

[CreateAssetMenu(menuName = "ScriptableObjects/ MasterManager")]
public class MasterManager : SingletonScriptableObject<MasterManager>
{
    [SerializeField] private GameSetting gameSetting;
    public GameSetting GameSetting { get { return Instance.gameSetting; } }
}

