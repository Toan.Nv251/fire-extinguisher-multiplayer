using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSetting", menuName = "ScriptableObjects/GameSetting")]
public class GameSetting : SingletonScriptableObject<GameSetting>
{
    [SerializeField] private string gameVersion = "0.0.0";
    public string GameVersion {get {return gameVersion;}}
    [SerializeField] private string nickName = "Player";
    public string NickName
    {
        get
        {
            int value = Random.Range(0,9999);
            return nickName + value.ToString();
        }
    }
}
