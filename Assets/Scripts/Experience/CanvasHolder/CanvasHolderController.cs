using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasHolderController : MonoBehaviour
{
    #region Singleton pattern
    private static CanvasHolderController instance;
    public static CanvasHolderController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<CanvasHolderController>();
            }
            return instance;
        }
    }
    #endregion Singleton pattern
    [SerializeField] private GameObject welcomeUI;
    [SerializeField] private GameObject step1UI;
    [SerializeField] private GameObject step2UI;
    [SerializeField] private GameObject doneUI;
    [SerializeField] private PlayerController playerController;
    private bool welcome = false;
    public bool Welcome { get { return welcome; } }
    private bool step1 = false;
    private bool step2 = false;
    private bool done = false;
    [SerializeField] private Camera cam;
    void Start()
    {
        welcome = true;
    }

    void Update()
    {
        transform.rotation = cam.transform.rotation;
        ValueBoolUIControl();
        UIControl();
    }
    private void WelcomeUI()
    {
        welcomeUI.gameObject.SetActive(true);
        step1UI.gameObject.SetActive(false);
        step2UI.gameObject.SetActive(false);
        doneUI.gameObject.SetActive(false);
    }
    private void Step1UI()
    {
        welcomeUI.gameObject.SetActive(false);
        step1UI.gameObject.SetActive(true);
        step2UI.gameObject.SetActive(false);
        doneUI.gameObject.SetActive(false);
    }
    private void Step2UI()
    {
        welcomeUI.gameObject.SetActive(false);
        step1UI.gameObject.SetActive(false);
        step2UI.gameObject.SetActive(true);
        doneUI.gameObject.SetActive(false);
    }
    private void DoneUI()
    {
        welcomeUI.gameObject.SetActive(false);
        step1UI.gameObject.SetActive(false);
        step2UI.gameObject.SetActive(false);
        doneUI.gameObject.SetActive(true);
    }
    private void UIControl()
    {
        if(welcome)
        {
            WelcomeUI();
        }
        else if (step1)
        {
            Step1UI();
        }
        else if (step2)
        {
            Step2UI();
        }
        else if (done)
        {
            DoneUI();
        }
    }
    private void ValueBoolUIControl()
    {
        if(welcome && Input.GetKeyDown("o"))
        {
            welcome = false;
            step1 = true;
            step2 = false;
            done = false;
        }
        if(!welcome && playerController.IsHolding)
        {
            welcome = false;
            step1 = false;
            step2 = true;
            done = false;
        }
        else if (!welcome && !playerController.IsHolding)
        {
            welcome = false;
            step1 = true;
            step2 = false;
            done = false;
        }
        if(!FireController.Instance.IsLit)
        {
            welcome = false;
            step1 = false;
            step2 = false;
            done = true;
        }
    }
}
