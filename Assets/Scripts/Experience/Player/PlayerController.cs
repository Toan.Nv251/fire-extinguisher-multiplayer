using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using UnityEngine.Animations;

public class PlayerController : MonoBehaviourPun
{
    #region Singleton pattern
    private static PlayerController instance;
    public static PlayerController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<PlayerController>();
            }
            return instance;
        }
    }
    #endregion Singleton pattern
    // Move camera by mouse
    [Header("Move Camera")]
    [SerializeField] private Transform playerCamera;
    [SerializeField] private Transform cameraHolder;
    [SerializeField] private float senX;
    [SerializeField] private float senY;
    [SerializeField] private Transform orientation;
    private float xRotation;
    private float yRotation;
    // Player Movement
    [Header("Player Movement")]
    [SerializeField] private float moveSpeed;
    [SerializeField] private LayerMask isGround;
    [SerializeField] private float PlayerHeight;
    private Vector3 moveDirection;
    private float horizontalInput;
    private float verticalInput;
    private bool grounded;
    private Rigidbody playerRigidbody;
    // Pickup and drop
    [SerializeField] private float maxDistancePickUp = 2f;
    [SerializeField] private GameObject itemHolder;
    private GameObject currentObjectHolding;
    private bool isHolding;
    public bool IsHolding { get { return isHolding; } }
    [SerializeField] private bool isSafetyPinRemoved;
    public bool IsSafetyPinRemoved { get { return isSafetyPinRemoved; } }
    // Control Fire Extinguisher
    private FireExtinguisherController fireExtinguisherController;
    private bool canFireFighting = true;
    public bool CanFireFighting { get { return canFireFighting; } set { canFireFighting = value; } }
    // Control canvas
    [SerializeField] private GameObject playerCanvas;
    public GameObject PlayerCanvas { get { return playerCanvas; } }
    // Control animation
    [SerializeField] private Animator workerAnimator;
    [SerializeField] private Transform worker;
    [SerializeField] private Transform workderHolder;
    [SerializeField] private CanvasHolderController canvasHolderController;
    [SerializeField] private PhotonView pv;
    private ConstraintSource constraintSource;
    private void Start()
    {
        if (!pv.IsMine)
        {
            GetComponentInChildren<Camera>().gameObject.SetActive(false);
        }
        playerRigidbody = GetComponent<Rigidbody>();
        playerRigidbody.freezeRotation = true;
        // Lock cursor when play scenen
        /*Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;*/
        canvasHolderController.gameObject.transform.parent = null;
    }

    private void Update()
    {
        if (pv.IsMine)
        {
            MovePlayerCamera();
            CameraMovement();
            InputMovePlayer();
            GroundCheck();
            LimitSpeed();
            // Pick and Drop object
            if (Input.GetKeyDown("e"))
            {
                PickUp();
            }
            else if (Input.GetKeyDown("q"))
            {
                Drop();
            }
            if (Input.GetKeyDown("r") && isHolding == true)
            {
                /*ReleasePin();*/
                ReleasePin();

            }

            // Control Particles system
            if (Input.GetMouseButtonDown(0) && isSafetyPinRemoved == true && canFireFighting)
            {
                PlayFireExtinguisher();
            }
            else if (Input.GetMouseButtonUp(0) && isSafetyPinRemoved == true || !canFireFighting)
            {
                StopFireExtinguisher();
            }
            // Put out a fire
            PutOutFire();
            // Reset Scene
            if (Input.GetKeyDown("o") && !FireController.Instance.IsLit)
            {
                ResetScene();
            }
            // Control animation
            ControlAnimation();
        }
    }
    private void FixedUpdate()
    {
        MovePlayer();
    }
    public void ReleasePin()
    {
        if (fireExtinguisherController != null)
        {
            if (fireExtinguisherController.IsDropSafetyPin)
            {
                return;
            }
            isSafetyPinRemoved = true;
            fireExtinguisherController.CallReleasePin();
        }
    }
    private void CameraMovement()
    {
        cameraHolder.position = playerCamera.position;
    }
    private void MovePlayerCamera()
    {
        // Get mouse input
        float mouseX = Input.GetAxisRaw("Mouse X") * Time.deltaTime * senX;
        float mouseY = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * senY;
        yRotation += mouseX;
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        workderHolder.rotation = Quaternion.Euler(0, yRotation, 0);
        // Rotation player camera and orientation
        playerCamera.transform.rotation = Quaternion.Euler(xRotation, yRotation, 0);
        orientation.transform.rotation = Quaternion.Euler(0, yRotation, 0);
    }
    private void InputMovePlayer()
    {
        if(!canvasHolderController.Welcome)
        {
            horizontalInput = Input.GetAxisRaw("Horizontal");
            verticalInput = Input.GetAxisRaw("Vertical");
        }
    }
    private void GroundCheck()
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, PlayerHeight * 0.5f + 0.3f, isGround);
    }
    private void MovePlayer()
    {
        // Calculate movement direction
        moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;
        if(grounded)
        {
            playerRigidbody.AddForce(moveDirection.normalized * 10f * moveSpeed, ForceMode.Force);
        }
    }
    private void LimitSpeed()
    {
        Vector3 currentSpeed = new Vector3(playerRigidbody.velocity.x, 0f, playerRigidbody.velocity.z);
        if(currentSpeed.magnitude > moveSpeed)
        {
            Vector3 newSpeed = currentSpeed.normalized * moveSpeed;
            playerRigidbody.velocity = new Vector3(newSpeed.x, playerRigidbody.velocity.y, newSpeed.z);
        }
    }
    public void PickUp()
    {
        RaycastHit thisHit;
        if (!isHolding)
        {
            if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out thisHit, maxDistancePickUp))
            {
                if (thisHit.transform.tag == "FireExtinguisher")
                {
                    fireExtinguisherController = thisHit.transform.GetComponentInChildren<FireExtinguisherController>();
                    fireExtinguisherController.CallChangeStateArrow();
                    currentObjectHolding = thisHit.transform.gameObject;
                    fireExtinguisherController.IsHolding = true;
                    fireExtinguisherController.CallChangeStateFireExtinguisher(true);
                    constraintSource.sourceTransform = itemHolder.transform;
                    constraintSource.weight = 1f;
                    var parentConstraint = currentObjectHolding.GetComponent<ParentConstraint>();
                    parentConstraint.AddSource(constraintSource);
                    currentObjectHolding.transform.localPosition = Vector3.zero;
                    currentObjectHolding.transform.localEulerAngles = Vector3.zero;
                    isHolding = true;
                    if (!fireExtinguisherController.IsDropSafetyPin)
                    {
                        isSafetyPinRemoved = false;
                    }
                    else
                    {
                        isSafetyPinRemoved = true;
                    }
                }
            }
        }
        else
        {
            return;
        }
    }
    private void Drop()
    {
        if(isHolding && !fireExtinguisherController.FireExtinguisherPS.isPlaying)
        {
            fireExtinguisherController.CallChangeStateFireExtinguisher(false);
            fireExtinguisherController = null;
            currentObjectHolding.transform.parent = null;
            var parentConstraint = currentObjectHolding.GetComponent<ParentConstraint>();
            parentConstraint.RemoveSource(0);
            isHolding = false;
        }
        else
        {
            return;
        }
    }
    private void PlayFireExtinguisher()
    {
        if(fireExtinguisherController != null)
        {
            /*fireExtinguisherController.FireExtinguisherPS.Play();*/
            fireExtinguisherController.CallChangeStatePS(fireExtinguisherController.IsPlaying = true);
        }
    }
    private void StopFireExtinguisher()
    {
        if (fireExtinguisherController != null)
        {
            /*fireExtinguisherController.FireExtinguisherPS.Stop();*/
            fireExtinguisherController.CallChangeStatePS(fireExtinguisherController.IsPlaying = false);
        }
    }
    private void PutOutFire()
    {
        if(fireExtinguisherController != null)
        {
            if(fireExtinguisherController.FireExtinguisherPS.isPlaying)
            {
                fireExtinguisherController.ColliderRaycast();
            }
        }
    }
    private void ResetScene()
    {
        SceneManager.LoadScene("Experience");
    }
    private void ControlAnimation()
    {
        worker.rotation = Quaternion.Euler(0, yRotation, 0);
        if (moveDirection != Vector3.zero)
        {
            workerAnimator.SetBool("IsMoving", true);
        }
        else
        {
            workerAnimator.SetBool("IsMoving", false);
        }
    }
}
