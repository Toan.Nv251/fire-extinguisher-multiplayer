using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class FireController : MonoBehaviourPun
{
    #region Singleton pattern
    private static FireController instance;
    public static FireController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<FireController>();
            }
            return instance;
        }
    }
    #endregion Singleton pattern
    [SerializeField] private float currentIntensity;
    [SerializeField] private ParticleSystem[] fireParticleSystem = new ParticleSystem[0];
    [SerializeField] private float regenDelay = 2.5f;
    [SerializeField] private float regenRate = 0.1f;
    private float[] startIntensities = new float[0];
    private float timeLastWatered = 0;
    [SerializeField] private bool isLit = true;
    public bool IsLit { get { return isLit; } }
    void Start()
    {
        SetArrayParticleSystem();
    }
    private void Update()
    {
        if (isLit && currentIntensity < 1.0f && Time.time - timeLastWatered >= regenDelay)
        {
            currentIntensity += regenRate * Time.deltaTime;
            ChangeIntensity();
        }
        if(currentIntensity < 0)
        {
            isLit = false;
        }
        if(!isLit)
        {
            photonView.RPC("WhenIsLit", RpcTarget.AllBuffered);
        }
    }
    private void SetArrayParticleSystem()
    {
        startIntensities = new float[fireParticleSystem.Length];
        for (int i = 0; i < fireParticleSystem.Length; i++)
        {
            startIntensities[i] = fireParticleSystem[i].emission.rateOverTime.constant;
        }
    }
    private void ChangeIntensity()
    {
        for (int i = 0; i < fireParticleSystem.Length; i++)
        {
            var emission = fireParticleSystem[i].emission;
            emission.rateOverTime = currentIntensity * startIntensities[i];
        }
    }
    public void CallTryExtinguish(float amount)
    {
        photonView.RPC("TryExtinguish", RpcTarget.AllBuffered, amount);
    }
    [PunRPC]
    public bool TryExtinguish(float amount)
    {
        timeLastWatered = Time.time;
        currentIntensity -= amount;
        ChangeIntensity();
        if(currentIntensity <= 0)
        {
            isLit = false;
            return true;
        }
        return false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<PlayerController>() && isLit)
        {
            other.gameObject.GetComponent<PlayerController>().PlayerCanvas.SetActive(true);
            other.gameObject.GetComponent<PlayerController>().CanFireFighting = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.GetComponent<PlayerController>() && isLit)
        {
            other.gameObject.GetComponent<PlayerController>().PlayerCanvas.SetActive(false);
            other.gameObject.GetComponent<PlayerController>().CanFireFighting = true;
        }
    }
    [PunRPC]
    public void WhenIsLit()
    {
        for(int i = 0; i < fireParticleSystem.Length; i++)
        {
            fireParticleSystem[i].gameObject.SetActive(false);
        }
    }
}
