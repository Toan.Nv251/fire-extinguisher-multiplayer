using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using UnityEngine.Animations;

public class FireExtinguisherController : MonoBehaviourPun
{
    public ParticleSystem FireExtinguisherPS;
    [SerializeField] private GameObject waterMist;
    [SerializeField] private float amountExtinguishedPerSecond;
    [SerializeField] private GameObject safetyPin;
    public GameObject SafetyPin { get { return safetyPin; } }
    [SerializeField] private GameObject arrow;
    public GameObject Arrow { get { return arrow; } }
    private bool isHolding;
    public bool IsHolding { get { return isHolding; } set { isHolding = value; } }
    private Vector3 forward;
    private bool isDropSafetyPin = false;
    public bool IsDropSafetyPin { get { return isDropSafetyPin; }  set { isDropSafetyPin = value; } }
    private RaycastHit hit;
    public RaycastHit Hit { get { return hit; } }
    private bool isPlaying = false;
    public bool IsPlaying { get { return isPlaying; } set { isPlaying = value; } }
    private void Update()
    {
        DrawRayCast();
    }
    private void DrawRayCast()
    {
        forward = waterMist.transform.TransformDirection(-Vector3.up * 8);
        Debug.DrawRay(waterMist.transform.position, forward, Color.green);
    }
    public void ColliderRaycast()
    {
        if(Physics.Raycast(waterMist.transform.position, forward, out hit, 8))
        {
            if(hit.transform.tag == "Fire" && hit.collider.TryGetComponent(out FireController fire))
            {
                fire.CallTryExtinguish(amountExtinguishedPerSecond * Time.deltaTime);
            }
        }
    }
    public void CallChangeStatePS(bool isPlaying)
    {
        photonView.RPC("ChangeStatePS", RpcTarget.All, isPlaying);
    }
    [PunRPC]
    public void ChangeStatePS(bool isPlaying)
    {
        if(isPlaying)
        {
            FireExtinguisherPS.Play();
        }
        else
        {
            FireExtinguisherPS.Stop();
        }
    }
    public void CallChangeStateArrow()
    {
        photonView.RPC("ChangeStateArrow", RpcTarget.AllBuffered);
    }
    [PunRPC]
    public void ChangeStateArrow()
    {
        arrow.gameObject.SetActive(false);
    }
    public void CallChangeStateFireExtinguisher(bool holding)
    {
        photonView.RPC("ChangeStateFireExtinguisher", RpcTarget.AllBuffered, holding);
    }
    [PunRPC]
    public void ChangeStateFireExtinguisher(bool holding)
    {
        if(holding)
        {
            foreach (var colli in transform.GetComponentsInChildren<Collider>())
            {
                if (colli != null)
                {
                    colli.enabled = false;
                }
            }
            foreach (var rigid in transform.GetComponentsInChildren<Rigidbody>())
            {
                if (rigid != null)
                {
                    rigid.isKinematic = true;
                }
            }
        }
        else
        {
            foreach (var colli in transform.GetComponentsInChildren<Collider>())
            {
                if (colli != null)
                {
                    colli.enabled = true;
                }
            }
            foreach (var rigid in transform.GetComponentsInChildren<Rigidbody>())
            {
                if (rigid != null)
                {
                    rigid.isKinematic = false;
                }
            }
        }
    }
    public void CallReleasePin()
    {
        photonView.RPC("ReleasePin", RpcTarget.AllBuffered);
    }
    [PunRPC]
    public void ReleasePin()
    {
        if(IsDropSafetyPin)
        {
            return;
        }
        Vector3 startLocalPosition = new Vector3(0, 0, 0);
        Vector3 endLocalPosition = new Vector3(-0.005f, 0, 0);
        /*currentObjectHolding.transform.GetChild(0).GetChild(3).transform.localPosition = Vector3.Lerp(startLocalPosition, endLocalPosition, 1f);*/
        SafetyPin.gameObject.transform.localPosition = Vector3.Lerp(startLocalPosition, endLocalPosition, 1f);
        IsDropSafetyPin = true;
        foreach (var colli in SafetyPin.gameObject.transform.GetComponentsInChildren<Collider>())
        {
            if (colli != null)
            {
                colli.enabled = true;
            }
        }
        SafetyPin.AddComponent<Rigidbody>();
    }
}
