using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Laucher : MonoBehaviourPunCallbacks
{
    public PhotonView playerPrefab;
    public PhotonView fireExtinguisher;
    public PhotonView campFire;
    [SerializeField] private Camera cam;
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    void Update()
    {

    }
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("Connected to master");
        PhotonNetwork.JoinRandomOrCreateRoom();
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("Joined a room successfully");
        if(PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.Instantiate(campFire.name, Vector3.zero, Quaternion.identity);
            PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(0f, 1f, -16f), Quaternion.identity);
            PhotonNetwork.Instantiate(fireExtinguisher.name, new Vector3(2f, 0.8424f, -7.47f), Quaternion.identity);
        }
        else
        {
            PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(10f, 1f, -16f), Quaternion.identity);
            PhotonNetwork.Instantiate(fireExtinguisher.name, new Vector3(8f, 0.8424f, -7.47f), Quaternion.identity);
        }
        Destroy(cam.gameObject);
    }
}
