using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Lesson
{
    public string lessonId;
    public string lessonTitle;
    public int categoryId;
    public string lessonDescription;
    public string lessonThumbnail;
}

[System.Serializable]
public class Lessons
{
    public Lesson[] categorywithLessons;
}


