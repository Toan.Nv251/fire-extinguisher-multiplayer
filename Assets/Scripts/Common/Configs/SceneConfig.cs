using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SceneConfig
{
    public static string start = "Start";
    public static string signIn = "SignIn";
    public static string home = "Home";
    public static string experience = "Experience";
}
